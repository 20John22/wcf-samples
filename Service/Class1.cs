﻿using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;

namespace Service
{
	public class MyUserNamePasswordValidator : UserNamePasswordValidator
	{
		public override void Validate(string userName, string password)
		{
			if (userName == "usr" && password == "usr")
				return;
			throw new SecurityTokenException("Unknown user.");
		}
	}
}