﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.Xml;

using Core.Extensions;

namespace Core
{
	[DataContract]
	public class SoapHeader : MessageHeader
	{
		private string _userName;
		private string _password;

		public string UserName
		{
			get
			{
				return (this._userName);
			}
			set
			{
				this._userName = value;
			}
		}

		public string Password
		{
			get
			{
				return (this._password);
			}
			set
			{
				this._password = value;
			}
		}

		public SoapHeader() { }
		public SoapHeader(string userName, string password)
		{
			UserName = userName;
			Password = password;
		}

		public override string Name
		{
			get { return (SoapHeaderNames.SoapHeaderName); }
		}

		public override string Namespace
		{
			get { return (SoapHeaderNames.SoapHeaderNamespace); }
		}
		protected override void OnWriteHeaderContents(System.Xml.XmlDictionaryWriter writer, MessageVersion messageVersion)
		{
			// Write the content of the header directly using the XmlDictionaryWriter
			writer.WriteElementString(SoapHeaderNames.UserName, this.UserName);
			writer.WriteElementString(SoapHeaderNames.Password, this.Password);
		}

		public static SoapHeader ReadHeader(XmlDictionaryReader reader)
		{
			String userName = null;
			String password = null;

			// Read the header content (key) using the XmlDictionaryReader
			if (reader.ReadToDescendant(SoapHeaderNames.UserName, SoapHeaderNames.SoapHeaderNamespace))
			{
				userName = reader.ReadElementString();
				password = reader.ReadElementString();
			}

			//if (reader.ReadToFollowing(SoapHeaderNames.Password, SoapHeaderNames.SoapHeaderNamespace))
			//{
			//	password = reader.ReadElementString();
			//}

			if (!String.IsNullOrEmpty(userName) && !String.IsNullOrEmpty(password))
			{
				return new SoapHeader(userName, password);
			}
			else
			{
				return null;
			}
		}
	}
}
