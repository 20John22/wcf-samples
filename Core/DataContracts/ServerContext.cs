﻿using System.ServiceModel;

namespace Core
{
	public class ServerContext : IExtension<OperationContext>
	{
		public string UserName;
		public string Password;

		// Get the current one from the extensions that are added to OperationContext.
		public static ServerContext Current
		{
			get
			{
				return OperationContext.Current.Extensions.Find<ServerContext>();
			}
		}

		#region IExtension<OperationContext> Members
		public void Attach(OperationContext owner)
		{
		}

		public void Detach(OperationContext owner)
		{
		}
		#endregion
	}

}
