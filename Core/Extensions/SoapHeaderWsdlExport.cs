﻿/*
 * Source code copied from http://blogs.msdn.com/b/skaufman/archive/2009/05/29/exposing-custom-wcf-headers-through-wcf-behaviors.aspx
 */

using System;
using System.Reflection;
using System.ServiceModel.Description;
using System.Web.Services.Description;
using System.Xml;
using System.Xml.Schema;

using WsdlDescription = System.Web.Services.Description.ServiceDescription;

namespace Core.Extensions
{
	class SoapHeaderWsdlExport
	{
		public static void ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
		{
			// Read the schema of the custom header message
			XmlSchema customSoapHeaderSchema = XmlSchema.Read(Assembly.GetExecutingAssembly().GetManifestResourceStream("Core.SoapHeader.xsd"),
				new ValidationEventHandler(SoapHeaderWsdlExport.ValidationCallBack));

			// Create the HeaderMessage to add to wsdl:message AND to refer to from wsdl:operation
			System.Web.Services.Description.Message headerMessage = CreateHeaderMessage();

			foreach (WsdlDescription wsdl in exporter.GeneratedWsdlDocuments)
			{
				// Add the schema of the CustomSoapHeader to the types AND add the namespace to the list of namespaces
				wsdl.Types.Schemas.Add(customSoapHeaderSchema);
				wsdl.Namespaces.Add("sh", SoapHeaderNames.SoapHeaderNamespace);

				// The actual adding of the message to the list of messages
				wsdl.Messages.Add(headerMessage);
			}

			addHeaderToOperations(headerMessage, context);
		}

		private static System.Web.Services.Description.Message CreateHeaderMessage()
		{
			// Create Message
			System.Web.Services.Description.Message headerMessage = new System.Web.Services.Description.Message();

			// Set the name of the header message
			headerMessage.Name = SoapHeaderNames.SoapHeaderName;

			// Create the messagepart and add to the header message
			MessagePart part = new MessagePart();
			part.Name = "Header";
			part.Element = new XmlQualifiedName(SoapHeaderNames.SoapHeaderName, SoapHeaderNames.SoapHeaderNamespace);
			headerMessage.Parts.Add(part);

			return headerMessage;
		}

		private static void addHeaderToOperations(System.Web.Services.Description.Message headerMessage, WsdlEndpointConversionContext context)
		{
			// Create a XmlQualifiedName based on the header message, this will be used for binding the header message and the SoapHeaderBinding
			XmlQualifiedName header = new XmlQualifiedName(headerMessage.Name, headerMessage.ServiceDescription.TargetNamespace);

			foreach (OperationBinding operation in context.WsdlBinding.Operations)
			{
				// Add the SoapHeaderBinding to the MessageBinding
				ExportMessageHeaderBinding(operation.Input, context, header, false);
				//ExportMessageHeaderBinding(operation.Output, context, header, false);
			}
		}

		private static void ExportMessageHeaderBinding(MessageBinding messageBinding, WsdlEndpointConversionContext context, XmlQualifiedName header, bool isEncoded)
		{
			// For brevity, assume Soap12HeaderBinding for Soap 1.2
			SoapHeaderBinding binding = new Soap12HeaderBinding();
			binding.Part = "Header";
			binding.Message = header;
			binding.Use = isEncoded ? SoapBindingUse.Encoded : SoapBindingUse.Literal;

			messageBinding.Extensions.Add(binding);
		}

		private static void ValidationCallBack(object sender, ValidationEventArgs args)
		{
			if (args.Severity == XmlSeverityType.Warning)
				Console.WriteLine("\tWarning: Matching schema not found. No validation occurred." + args.Message);
			else
				Console.WriteLine("\tValidation error: " + args.Message);

		}
	}
}
