﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Xml;

namespace Core.Extensions
{
	public class SoapHeaderMessageInspector : IClientMessageInspector, IDispatchMessageInspector
	{
		#region Message Inspector of the Service

		/// <summary>
		/// This method is called on the server when a request is received from the client.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="channel"></param>
		/// <param name="instanceContext"></param>
		/// <returns></returns>
		object IDispatchMessageInspector.AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
		{
			//SoapHeader header			= request.Headers.GetHeader<SoapHeader>(SoapHeaderNames.SoapHeaderName, SoapHeaderNames.SoapHeaderNamespace);
			
			//// Look for my custom header in the request
			Int32 headerPosition = OperationContext.Current.IncomingMessageHeaders.FindHeader(SoapHeaderNames.SoapHeaderName, SoapHeaderNames.SoapHeaderNamespace);

			//// Get an XmlDictionaryReader to read the header content
			XmlDictionaryReader reader = OperationContext.Current.IncomingMessageHeaders.GetReaderAtHeader(headerPosition);

			//// Read through its static method ReadHeader
			SoapHeader header = SoapHeader.ReadHeader(reader);


			ServerContext serverContext = new ServerContext();

			if (header != null)
			{
				serverContext.UserName	= header.UserName;
				serverContext.Password	= header.Password;
			}
			OperationContext.Current.Extensions.Add(serverContext);
			return null;
		}

		/// <summary>
		/// This method is called after processing a method on the server side and just
		/// before sending the response to the client.
		/// </summary>
		/// <param name="reply"></param>
		/// <param name="correlationState"></param>
		void IDispatchMessageInspector.BeforeSendReply(ref Message reply, object correlationState)
		{
			OperationContext.Current.Extensions.Remove(ServerContext.Current);
		}

		#endregion

		#region Message Inspector of the Consumer

		/// <summary>
		/// This method will be called from the client side just before any method is called.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="channel"></param>
		/// <returns></returns>
		object IClientMessageInspector.BeforeSendRequest(ref Message request, IClientChannel channel)
		{
			SoapHeader header			= new SoapHeader();
			header.UserName				= ClientContext.UserName;
			header.Password				= ClientContext.Password;

			//MessageHeader<SoapHeader> typedHeader	= new MessageHeader<SoapHeader>(header);
			//MessageHeader untypedHeader				= typedHeader.GetUntypedHeader(SoapHeaderNames.SoapHeaderName, SoapHeaderNames.SoapHeaderNamespace);

			request.Headers.Add(header);
			return null;
		}

		/// <summary>
		/// This method will be called after completion of a request to the server.
		/// </summary>
		/// <param name="reply"></param>
		/// <param name="correlationState"></param>
		void IClientMessageInspector.AfterReceiveReply(ref Message reply, object correlationState)
		{

		}

		#endregion

	}
}
