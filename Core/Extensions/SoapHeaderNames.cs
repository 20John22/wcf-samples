﻿using System;

namespace Core.Extensions
{
	public class SoapHeaderNames
	{
		public const String SoapHeaderName	= "SoapUserNameHeader";
		public const String UserName		= "UserName";
		public const String Password		= "Password";
		public const String SoapHeaderNamespace = @"http://servicebehavior.mycompany.com"; 
	}
}
