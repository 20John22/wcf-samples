﻿using System;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Core.Extensions
{
	public class SoapHeaderEndpointBehavior : BehaviorExtensionElement, IEndpointBehavior, IWsdlExportExtension
	{
		#region Behavior Extension Element Members

		protected override object CreateBehavior()
		{
			return new SoapHeaderEndpointBehavior();
		}
		public override Type BehaviorType
		{
			get { return typeof(SoapHeaderEndpointBehavior); }
		}

		#endregion

		#region IEndpointBehavior Members

		public void AddBindingParameters(ServiceEndpoint endpoint,
			 System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
		{
		}

		public void ApplyClientBehavior(ServiceEndpoint endpoint,
				 System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
		{
			SoapHeaderMessageInspector inspector = new SoapHeaderMessageInspector();
			clientRuntime.MessageInspectors.Add(inspector);
		}

		public void ApplyDispatchBehavior(ServiceEndpoint endpoint,
				 System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
		{
			ChannelDispatcher channelDispatcher = endpointDispatcher.ChannelDispatcher;
			if (channelDispatcher != null)
			{
				foreach (EndpointDispatcher ed in channelDispatcher.Endpoints)
				{
					SoapHeaderMessageInspector inspector = new SoapHeaderMessageInspector();
					ed.DispatchRuntime.MessageInspectors.Add(inspector);
				}
			}
		}

		public void Validate(ServiceEndpoint endpoint)
		{
		}

		#endregion

		#region IWsdlExportExtension Members

		public void ExportContract(WsdlExporter exporter, WsdlContractConversionContext context)
		{
		}

		public void ExportEndpoint(WsdlExporter exporter, WsdlEndpointConversionContext context)
		{
			SoapHeaderWsdlExport.ExportEndpoint(exporter, context);
		}

		#endregion
	}
}
